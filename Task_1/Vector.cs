﻿using System;

namespace Task_1
{
    public class Vector : IComparable
    {
        private readonly int startIndex;
        private readonly int realSize;
        private readonly int[] vector;

        public Vector(int startIndex, params int[] vector)
        {
            this.vector = new int[startIndex + vector.Length];
            this.startIndex = startIndex;
            realSize = this.vector.Length - this.startIndex;

            for (int i = startIndex; i < this.vector.Length; i++)
            {
                this.vector[i] = vector[i - startIndex];
            }
        }

        public int this[int i]
        {
            get
            {
                if ((i < startIndex) || (i > vector.Length))
                {
                    throw new IndexOutOfRangeException();
                }

                return vector[i];
            }

            set
            {
                if ((i < startIndex) || (i > vector.Length))
                {
                    throw new IndexOutOfRangeException();
                }

                vector[i] = value;
            }
        }

        public static Vector operator +(Vector left, Vector right)
        {
            if ((left == null) || (right == null))
            {
                throw new NullReferenceException("One of vectors is null");
            }

            if (left.realSize != right.realSize)
            {
                throw new Exception("Vectors have a difirent intervals");
            }

            Vector returnValue = new Vector(0, new int[left.realSize]);

            for (int i = 0; i < returnValue.realSize; i++)
            {
                returnValue[i] = left.vector[i + left.startIndex] + right.vector[i + right.startIndex];
            }

            return returnValue;
        }

        public static Vector operator -(Vector left, Vector right)
        {
            if ((left == null) || (right == null))
            {
                throw new NullReferenceException("One of vectors is null");
            }

            if (left.realSize != right.realSize)
            {
                throw new Exception("Vectors have a difirent intervals");
            }

            Vector returnValue = new Vector(0, new int[left.realSize]);

            for (int i = 0; i < returnValue.realSize; i++)
            {
                returnValue[i] = left.vector[i + left.startIndex] - right.vector[i + right.startIndex];
            }

            return returnValue;
        }

        public static Vector operator *(Vector vector, int scalar)
        {
            if ((vector == null) || (scalar == 0))
            {
                throw new Exception("vector = null or scalar < 1");
            }

            for (int i = vector.startIndex; i < vector.vector.Length; i++)
            {
                vector[i] *= scalar;
            }

            return vector;
        }

        public int CompareTo(object obj)
        {
            Vector inVector = (Vector)obj;

            if (realSize > inVector.realSize)
            {
                return 1;
            }

            if (realSize < inVector.realSize)
            {
                return -1;
            }

            if (realSize == inVector.realSize)
            {
                int position = -1;

                while (position + startIndex != realSize)
                {
                    position++;

                    if (vector[position + startIndex] > inVector.vector[position + inVector.startIndex])
                    {
                        return 1;
                    }

                    if (vector[position + startIndex] < inVector.vector[position + inVector.startIndex])
                    {
                        return -1;
                    }
                }
            }

            return 0;
        }

        public override string ToString()
        {
            string returnValue = null;

            for (int i = startIndex; i < vector.Length; i++)
            {
                returnValue += $"{vector[i]}, ";
            }

            if (returnValue == null)
            {
                throw new NullReferenceException();
            }

            return returnValue;
        }
    }
}
