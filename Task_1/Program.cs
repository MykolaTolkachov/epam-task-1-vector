﻿using static System.Console;

namespace Task_1
{
    internal class Program
    {
        static void Main()
        {
            SetWindowSize(52, 15);

            var vector1 = new Vector(3, 2, 3, 4, 5, 6);
            var vector2 = new Vector(4, 2, 3, 4, 5, 6);
            var vector3 = new Vector(5, 1, 2, 3, 4, 5);

            WriteLine("Vector 1: \t" + vector1);
            WriteLine("Vector 2: \t" + vector2);
            WriteLine("Vector 3: \t" + vector3);

            WriteLine(new string('-', 50));

            var vector4 = vector1 + vector2;
            WriteLine("Vector 4 = Vector 1 + Vector 2: " + vector4);

            var vector5 = vector1 - vector3;
            WriteLine("Vector 5 = Vector 1 - Vector 3: " + vector5);

            var vector6 = vector1 * 5;
            WriteLine("Vector 6 = Vector 1 * 5: \t" + vector6);
            WriteLine(new string('-', 50));

            var vector7 = new Vector(5, 1, 2, 3, 4, 5);
            var vector8 = new Vector(4, 1, 2, 3, 4, 5);
            var vector9 = new Vector(3, 2, 2, 3, 4, 5);

            WriteLine("Vector 7: \t" + vector7);
            WriteLine("Vector 8: \t" + vector8);
            WriteLine("Vector 9: \t" + vector9);
            WriteLine(new string('-', 50));

            int flag = vector7.CompareTo(vector8);
            WriteLine("vector 7 CompareTo vector 8: " + flag);

            flag = vector7.CompareTo(vector9);
            WriteLine("vector 7 CompareTo vector 9: " + flag);

            ReadKey();
        }
    }
}
